.. _core_device_general_settings:

General Settings
----------------

Configure the General Settings to configure the device plugin.

:Field Enrollment Enabled: Devices will enroll for the Field Service.

:Office Enrollemt Enabled: Devices will enroll for the Office Service.

.. image:: general_settings.png

